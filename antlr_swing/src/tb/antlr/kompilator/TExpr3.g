tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e}, deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> sum(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> min(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podst(p1={$i1.text}, p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer ++;} -> if(e1={$e1.st}, e2={$e2.st}, e3.st={$e3.st}, n={numer.toString()})
    ;
    